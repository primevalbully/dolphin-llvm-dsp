// Copyright (C) 2003 Dolphin Project.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 2.0.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License 2.0 for more details.

// A copy of the GPL 2.0 should have been included with the program.
// If not, see http://www.gnu.org/licenses/

// Official SVN repository and contact information can be found at
// http://code.google.com/p/dolphin-emu/

#include "DSPAnalyzer.h"
#include "DSPCore.h"
#include "DSPHost.h"
#include "DSPInterpreter.h"
#include "DSPLLVMEmitter.h"
#include "DSPMemoryMap.h"
#include "StringUtil.h"

#include <llvm/Analysis/Verifier.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#include <set>
#include <vector>

#define MAX_BLOCK_SIZE 512
#define DSP_IDLE_SKIP_CYCLES 0x1000

DSPLLVMEmitter::DSPLLVMEmitter()
	: ctx(llvm::getGlobalContext())
	, mod(new llvm::Module("DSPLLE LLVM Jit", ctx))
	, builder(ctx)
	, fpm(new llvm::FunctionPassManager(mod))
{
	llvm::InitializeNativeTarget();
	ee = llvm::EngineBuilder(mod).create();

	SetOptimizationPasses();

	compile_stub = GenerateStub();

	for (int i = 0; i < 0x10000; ++i)
	{
		llvm_functions[i] = NULL;
		compiled_functions[i] = compile_stub;
	}
}

DSPLLVMEmitter::~DSPLLVMEmitter()
{
	delete fpm;
	delete mod;
}

DSPLLVMCompiledCode DSPLLVMEmitter::GenerateStub()
{
	llvm::FunctionType* ft = llvm::FunctionType::get(
		llvm::Type::getVoidTy(ctx), false
	);
	llvm::Function* func = llvm::Function::Create(
		ft, llvm::Function::ExternalLinkage, "dsp_stub", mod
	);

	llvm::BasicBlock* entry = llvm::BasicBlock::Create(ctx, "entry", func);
	builder.SetInsertPoint(entry);

	// Compile the current function...
	GenFunctionCall((void*)&CompileCurrent, ft, "CompileCurrent", 0);
	// ... and jump to it!
	EmitNonlocalDispatcher();

	return (DSPLLVMCompiledCode)ee->getPointerToFunction(func);
}

void DSPLLVMEmitter::Compile(u16 start_addr)
{
	if (llvm_functions[start_addr])
		return;

	// Create a void func(void) object
	llvm::FunctionType* ft = llvm::FunctionType::get(
		llvm::Type::getVoidTy(ctx), false
	);
	llvm::Function* func = llvm::Function::Create(
		ft, llvm::Function::ExternalLinkage,
		StringFromFormat("dsp_%04x", start_addr),
		mod
	);

	// Create the entry basic block
	llvm::BasicBlock* entry = llvm::BasicBlock::Create(ctx, "entry", func);
	builder.SetInsertPoint(entry);

	// Create the dispatcher entry block (it will be generated later on) as well
	// as the return block.
	dispatcher = llvm::BasicBlock::Create(ctx, "dispatcher", func);
	return_block = llvm::BasicBlock::Create(ctx, "return", func);

	// Define the variable which will contain the number of elapsed cycled for
	// the dispatcher.
	elapsed_cycles = builder.CreateAlloca(
		llvm::IntegerType::get(ctx, 16), NULL, "elapsed_cycles_var"
	);

	// Set the current block compilation state
	current_block_start = start_addr;
	compile_pc = start_addr;
	instr_blocks.clear();

	// Start by jumping to the dispatcher: we don't necessarily start executing
	// at the first instruction of this function.
	builder.CreateBr(dispatcher);

	// Set if we need to fixup PC after the last instruction
	bool fixup_pc = false;

	// Generate code for each instruction
	u16 block_size = 0;
	while (compile_pc < start_addr + MAX_BLOCK_SIZE)
	{
		// Create a basic block for this instruction, and branch last block to
		// this one (if it's not the first block).
		std::string bb_name = StringFromFormat("instr_%04x", compile_pc);
		llvm::BasicBlock* bb = llvm::BasicBlock::Create(ctx, bb_name, func);
		if (compile_pc != start_addr)
			builder.CreateBr(bb);
		builder.SetInsertPoint(bb);

		// Insert the basic block in the current function map.
		instr_blocks[compile_pc] = bb;

		if (DSPAnalyzer::code_flags[compile_pc] & DSPAnalyzer::CODE_CHECK_INT)
			EmitExceptionCheck(block_size);

		UDSPInstruction inst = dsp_imem_read(compile_pc);
		const DSPOPCTemplate* opcode = GetOpTemplate(inst);
		EmitInstruction(inst);

		block_size++;
		compile_pc += opcode->size;

		fixup_pc = true;

		// The current instruction is the end of a loop block
		if (DSPAnalyzer::code_flags[compile_pc - opcode->size] & DSPAnalyzer::CODE_LOOP_END)
		{
			// Check if we need to handle looping stuff.
			// if (st[2] > 0 && st[3] > 0)
			//     store_pc()
			//     HandleLoop()
			//     goto return_block;
			llvm::BasicBlock* st2_gt_0 = llvm::BasicBlock::Create(ctx, "st2_gt_0", func);
			llvm::BasicBlock* st3_gt_0 = llvm::BasicBlock::Create(ctx, "st3_gt_0", func);
			llvm::BasicBlock* skip_loop = llvm::BasicBlock::Create(ctx, "skip_loop", func);
			llvm::Value* cmp;

			// Load st2 and compare it to 0
			llvm::Value* st2 = builder.CreateLoad(
				GetCppInt((void*)&g_dsp.r.st[2], 16, "st2_var"), "st2"
			);
			cmp = builder.CreateICmpSGT(st2, GenSInt16(0));
			builder.CreateCondBr(cmp, st2_gt_0, skip_loop);

			// Load st3 and compare it to 0
			builder.SetInsertPoint(st2_gt_0);
			llvm::Value* st3 = builder.CreateLoad(
				GetCppInt((void*)&g_dsp.r.st[3], 16, "st3_var"), "st3"
			);
			cmp = builder.CreateICmpSGT(st3, GenSInt16(0));
			builder.CreateCondBr(cmp, st3_gt_0, skip_loop);

			builder.SetInsertPoint(st3_gt_0);
			if (!opcode->branch)
			{
				llvm::Value* pc = GetCppInt((void*)&g_dsp.pc, 16, "pc_var");
				builder.CreateStore(GenUInt16(compile_pc), pc);
			}

			llvm::FunctionType* ft = llvm::FunctionType::get(
				llvm::Type::getVoidTy(ctx), false
			);
			GenFunctionCall((void*)&DSPInterpreter::HandleLoop, ft, "HandleLoop", 0);

			CheckIdleSkip();
			builder.CreateBr(return_block);

			builder.SetInsertPoint(skip_loop);
		}

		if (opcode->branch)
		{
			fixup_pc = false;
			if (opcode->uncond_branch)
				break;
			else if (!opcode->llvmFunc) // LLVM instrs already do that
			{
				// If PC changed because of this branch instruction, jump to
				// the return block, else continue.
				llvm::BasicBlock* pc_changed = llvm::BasicBlock::Create(
					ctx, "pc_changed", func
				);
				llvm::BasicBlock* pc_not_changed = llvm::BasicBlock::Create(
					ctx, "pc_not_changed", func
				);

				llvm::Value* pc = GetCppInt((void*)&g_dsp.pc, 16, "pc_var");
				pc = builder.CreateLoad(pc, "pc");
				llvm::Value* cmp = builder.CreateICmpNE(pc, GenUInt16(compile_pc));
				builder.CreateCondBr(cmp, pc_changed, pc_not_changed);

				builder.SetInsertPoint(pc_changed);
				CheckIdleSkip();
				builder.CreateBr(return_block);

				builder.SetInsertPoint(pc_not_changed);
			}
		}
	}

	// Store PC if the last instruction was not a branch
	if (fixup_pc)
	{
		llvm::Value* pc = GetCppInt((void*)&g_dsp.pc, 16, "pc_var");
		builder.CreateStore(GenUInt16(compile_pc), pc);
	}

	// Link the last block to the return block
	builder.CreateBr(return_block);

	// Set the current block end and emit the dispatcher
	current_block_end = compile_pc;
	EmitDispatcher();

	// Optimize the function
	fpm->run(*func);

	// Compile the IR to X86 code and set the table entries
	DSPLLVMCompiledCode comp = (DSPLLVMCompiledCode)ee->getPointerToFunction(func);
	for (u16 i = current_block_start; i < current_block_end; ++i)
	{
		llvm_functions[i] = func;
		compiled_functions[i] = comp;
	}
}

void DSPLLVMEmitter::CheckIdleSkip()
{
	if (!DSPHost_OnThread() && DSPAnalyzer::code_flags[current_block_start] & DSPAnalyzer::CODE_IDLE_SKIP)
		builder.CreateStore(GenUInt16(DSP_IDLE_SKIP_CYCLES), elapsed_cycles);
}

void DSPLLVMEmitter::EmitExceptionCheck(u16 elapsed_cycles)
{
	llvm::Function* curr_func = builder.GetInsertBlock()->getParent();

	llvm::BasicBlock* handle;
	llvm::BasicBlock* skip;

	handle = llvm::BasicBlock::Create(ctx, "handle_exceptions", curr_func);
	skip = llvm::BasicBlock::Create(ctx, "skip_exceptions", curr_func);

	llvm::Value* exceptions = GetCppInt((void*)&g_dsp.exceptions, 8,
	                                    "exceptions_var");
	exceptions = builder.CreateLoad(exceptions, "exceptions");
	llvm::Value* cmp = builder.CreateICmpNE(exceptions, GenUInt8(0));
	builder.CreateCondBr(cmp, handle, skip);

	// Handle the exceptions
	builder.SetInsertPoint(handle);

	// Set PC to its current value
	llvm::Value* pc = GetCppInt((void*)&g_dsp.pc, 16, "pc_var");
	builder.CreateStore(GenUInt16(compile_pc), pc);

	// Call i32 DSPCore_CheckExceptions(void)
	llvm::FunctionType* ft = llvm::FunctionType::get(
		llvm::IntegerType::get(ctx, 32), false
	);
	GenFunctionCall((void*)&DSPCore_CheckExceptions, ft,
	                "DSPCore_CheckExceptions", 0);

	// Jump to the return block
	builder.CreateStore(GenUInt16(elapsed_cycles), this->elapsed_cycles);
	builder.CreateBr(return_block);

	builder.SetInsertPoint(skip);
}

void DSPLLVMEmitter::EmitDispatcher()
{
	builder.SetInsertPoint(dispatcher);

	// Initialize the number of elapsed cycles to 0
	builder.CreateStore(GenUInt16(0), elapsed_cycles);

	llvm::Function* curr_func = builder.GetInsertBlock()->getParent();
	llvm::BasicBlock* dispatch;
	llvm::BasicBlock* halt;

	dispatch = llvm::BasicBlock::Create(ctx, "dispatch", curr_func);
	halt = llvm::BasicBlock::Create(ctx, "halt", curr_func);

	// Check if the DSP needs to halt
	llvm::Value* cr = GetCppInt((void*)&g_dsp.cr, 8, "cr_var");
	cr = builder.CreateLoad(cr, "cr");
	llvm::Value* cr_masked = builder.CreateAnd(cr, GenUInt8(CR_HALT), "cr_masked");
	llvm::Value* needs_halt = builder.CreateICmpNE(cr_masked, GenUInt8(0),
	                                               "needs_halt");
	builder.CreateCondBr(needs_halt, halt, dispatch);

	// Emit the real dispatch code: if the pc goes to a local basic block, jump
	// directly to it, else try to find a function to execute.
	llvm::BasicBlock* not_local = llvm::BasicBlock::Create(ctx, "not_local", curr_func);

	builder.SetInsertPoint(dispatch);
	llvm::Value* pc = builder.CreateLoad(GetCppInt((void*)&g_dsp.pc, 16, "pc_var"), "pc");
	llvm::SwitchInst* sw = builder.CreateSwitch(pc, not_local);
	for (u16 i = current_block_start; i < current_block_end; ++i)
	{
		if (instr_blocks[i])
			sw->addCase(GenUInt16(i), instr_blocks[i]);
	}

	// Emit the non local dispatcher
	builder.SetInsertPoint(not_local);
	EmitNonlocalDispatcher();

	// If there are still cycles to execute, loop back to the dispatcher, else
	// return.
	builder.SetInsertPoint(return_block);

	// cyclesLeft -= elapsed_cycles;
	llvm::Value* cycles_left = GetCppInt((void*)&cyclesLeft, 16, "cycles_left_var");
	llvm::Value* cycles_left_val = builder.CreateLoad(cycles_left, "cycles_left");
	llvm::Value* elapsed = builder.CreateLoad(elapsed_cycles, "elapsed_cycles");
	llvm::Value* remaining = builder.CreateSub(cycles_left_val, elapsed);
	builder.CreateStore(remaining, cycles_left);

	// if (remaining > 0) goto dispatch; else goto halt;
	llvm::Value* continue_run = builder.CreateICmpSGT(remaining, GenUInt16(0));
	builder.CreateCondBr(continue_run, dispatcher, halt);

	builder.SetInsertPoint(halt);
	builder.CreateRetVoid();
}

void DSPLLVMEmitter::EmitNonlocalDispatcher()
{
	// Emit the dispatcher which jumps to an instruction not in the current
	// function.
	llvm::Value* pc = builder.CreateLoad(GetCppInt((void*)&g_dsp.pc, 16, "pc_var"), "pc");

	llvm::Type* ft = llvm::FunctionType::get(llvm::Type::getVoidTy(ctx), false);
	llvm::PointerType* pft = llvm::PointerType::getUnqual(ft);
	llvm::ArrayType* apft = llvm::ArrayType::get(pft, 0x10000);
	llvm::GlobalVariable* comp_funcs = mod->getGlobalVariable("compiled_functions");
	if (!comp_funcs)
	{
		comp_funcs = new llvm::GlobalVariable(
			*mod, apft, false, llvm::GlobalValue::ExternalLinkage, 0, "compiled_functions"
		);
		ee->addGlobalMapping(comp_funcs, (void*)&compiled_functions);
	}
	pc = builder.CreateZExt(pc, llvm::IntegerType::get(ctx, 64));
	std::vector<llvm::Value*> gep_args;
	gep_args.push_back(GenUInt32(0)); // deref the pointer
	gep_args.push_back(pc);           // index the array
	llvm::Value* pfunc = builder.CreateInBoundsGEP(comp_funcs, gep_args);
	llvm::Value* func = builder.CreateLoad(pfunc, "func");
	builder.CreateCall(func)->setTailCall(true);
	builder.CreateRetVoid();
}

void DSPLLVMEmitter::EmitInstruction(UDSPInstruction inst)
{
	const DSPOPCTemplate* tinst = GetOpTemplate(inst);
	bool ext_is_jit = false;

	// Increase the elapsed cycles count
	llvm::Value* ec = builder.CreateLoad(elapsed_cycles, "elapsed_cycles");
	ec = builder.CreateAdd(ec, GenUInt16(tinst->size));
	builder.CreateStore(ec, elapsed_cycles);

	// Create the interpreter instruction handlers function type
	std::vector<llvm::Type*> args_types;
	args_types.push_back(llvm::IntegerType::get(ctx, 16)); // one i16 arg
	llvm::FunctionType* ft = llvm::FunctionType::get(
		llvm::Type::getVoidTy(ctx), args_types, false
	);

	// Emit the extended part
	if (tinst->extended)
	{
		int mask = 0xFF;
		if ((inst >> 12) == 0x3)
			mask = 0x7F;

		if (extOpTable[inst & mask]->llvmFunc)
		{
			(this->*extOpTable[inst & mask]->llvmFunc)(inst);
			ext_is_jit = true;
		}
		else
		{
			GenFunctionCall((void*)extOpTable[inst & mask]->intFunc, ft,
			                extOpTable[inst & mask]->name,
			                1, GenUInt16(inst));
		}
	}

	if (opTable[inst]->llvmFunc)
		(this->*opTable[inst]->llvmFunc)(inst);
	else
	{
		// If the instruction reads PC, update the variable
		if (opTable[inst]->reads_pc)
		{
			llvm::Value* pc = GetCppInt((void*)&g_dsp.pc, 16, "pc_var");
			builder.CreateStore(GenUInt16(compile_pc + 1), pc);
		}

		// Emit the instruction body
		GenFunctionCall((void*)opTable[inst]->intFunc, ft,
		                opTable[inst]->name, 1, GenUInt16(inst));
	}

	// Write back the register changed by the extended op
	if (tinst->extended)
	{
		if (!ext_is_jit)
		{
			ft = llvm::FunctionType::get(llvm::Type::getVoidTy(ctx), false);
			GenFunctionCall((void*)::applyWriteBackLog, ft, "applyWriteBackLog", 0);
		}
		else
		{
			// TODO: JIT-ed extended instructions handling
		}
	}
}

void DSPLLVMEmitter::ClearIRAM()
{
	std::set<llvm::Function*> deleted;

	for (int i = 0; i < 0x1000; ++i)
	{
		if (llvm_functions[i] && deleted.find(llvm_functions[i]) == deleted.end())
		{
			deleted.insert(llvm_functions[i]);
			llvm_functions[i]->eraseFromParent();
		}
		llvm_functions[i] = NULL;
		compiled_functions[i] = compile_stub;
	}
}

llvm::Value* DSPLLVMEmitter::GenFunctionCall(void* fptr, llvm::FunctionType* ft,
                                             const std::string& name,
                                             int nparams, ...)
{
	// Get arguments into a std::vector
	std::vector<llvm::Value*> args;
	va_list va;

	va_start(va, nparams);
	for (int i = 0; i < nparams; ++i)
		args.push_back(va_arg(va, llvm::Value*));
	va_end(va);

	std::string funcname = StringFromFormat("%s_%p", name.c_str(), fptr);
	llvm::Function* f = mod->getFunction(funcname);
	if (!f)
	{
		f = llvm::Function::Create(ft, llvm::Function::ExternalLinkage,
		                           funcname, mod);
		ee->addGlobalMapping(f, fptr);
	}

	llvm::CallInst* retval = builder.CreateCall(f, args);
	retval->setTailCall(true);
	return retval;
}

llvm::Value* DSPLLVMEmitter::GetCppInt(void* vptr, int bits,
                                       const std::string& name)
{
	// Declare a constant global pointer to addr
	llvm::IntegerType* ity = llvm::IntegerType::get(ctx, bits);

	std::string varname = StringFromFormat("%s_%p", name.c_str(), vptr);
	llvm::GlobalVariable* v = mod->getGlobalVariable(varname);
	if (!v)
	{
		v = new llvm::GlobalVariable(
			*mod, ity, false, llvm::GlobalValue::ExternalLinkage, 0, varname
		);
		ee->addGlobalMapping(v, vptr);
	}

	return v;
}

void DSPLLVMEmitter::SetOptimizationPasses()
{
	llvm::PassManagerBuilder pmb;
	pmb.OptLevel = 3;
	pmb.populateFunctionPassManager(*fpm);
}
