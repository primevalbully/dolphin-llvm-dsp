// Copyright (C) 2003 Dolphin Project.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 2.0.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License 2.0 for more details.

// A copy of the GPL 2.0 should have been included with the program.
// If not, see http://www.gnu.org/licenses/

// Official SVN repository and contact information can be found at
// http://code.google.com/p/dolphin-emu/

#ifdef USE_LLVM
#ifndef _DSPLLVMEMITTER_H
#define _DSPLLVMEMITTER_H

#include "DSPCommon.h"

#include <llvm/ExecutionEngine/JIT.h>
#include <llvm/LLVMContext.h>
#include <llvm/Module.h>
#include <llvm/PassManager.h>
#include <llvm/Support/IRBuilder.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Target/TargetData.h>

typedef u32 (*DSPLLVMCompiledCode)();

class DSPLLVMEmitter
{
public:
	DSPLLVMEmitter();
	~DSPLLVMEmitter();

	void Compile(u16 start_addr);
	void ClearIRAM();

	llvm::Function* llvm_functions[0x10000];
	DSPLLVMCompiledCode compiled_functions[0x10000];

	// Ext commands
	void l(const UDSPInstruction opc);
	void ln(const UDSPInstruction opc);
	void ls(const UDSPInstruction opc);
	void lsn(const UDSPInstruction opc);
	void lsm(const UDSPInstruction opc);
	void lsnm(const UDSPInstruction opc);
	void sl(const UDSPInstruction opc);
	void sln(const UDSPInstruction opc);
	void slm(const UDSPInstruction opc);
	void slnm(const UDSPInstruction opc);
	void s(const UDSPInstruction opc);
	void sn(const UDSPInstruction opc);
	void ld(const UDSPInstruction opc);
	void ldax(const UDSPInstruction opc);
	void ldn(const UDSPInstruction opc);
	void ldaxn(const UDSPInstruction opc);
	void ldm(const UDSPInstruction opc);
	void ldaxm(const UDSPInstruction opc);
	void ldnm(const UDSPInstruction opc);
	void ldaxnm(const UDSPInstruction opc);
	void mv(const UDSPInstruction opc);
	void dr(const UDSPInstruction opc);
	void ir(const UDSPInstruction opc);
	void nr(const UDSPInstruction opc);
	void nop(const UDSPInstruction opc) {}

	// Commands
	void dar(const UDSPInstruction opc);
	void iar(const UDSPInstruction opc);
	void subarn(const UDSPInstruction opc);
	void addarn(const UDSPInstruction opc);
	void sbclr(const UDSPInstruction opc);
	void sbset(const UDSPInstruction opc);
	void srbith(const UDSPInstruction opc);
	void lri(const UDSPInstruction opc);
	void lris(const UDSPInstruction opc);
	void mrr(const UDSPInstruction opc);
	void nx(const UDSPInstruction opc);

	// Branch
	void jcc(const UDSPInstruction opc);
	void jmprcc(const UDSPInstruction opc);
	void call(const UDSPInstruction opc);
	void callr(const UDSPInstruction opc);
	void ifcc(const UDSPInstruction opc);
	void ret(const UDSPInstruction opc);
	void rti(const UDSPInstruction opc);
	void halt(const UDSPInstruction opc);
	void loop(const UDSPInstruction opc);
	void loopi(const UDSPInstruction opc);
	void bloop(const UDSPInstruction opc);
	void bloopi(const UDSPInstruction opc);

	// Load/Store
	void srs(const UDSPInstruction opc);
	void lrs(const UDSPInstruction opc);
	void lr(const UDSPInstruction opc);
	void sr(const UDSPInstruction opc);
	void si(const UDSPInstruction opc);
	void lrr(const UDSPInstruction opc);
	void lrrd(const UDSPInstruction opc);
	void lrri(const UDSPInstruction opc);
	void lrrn(const UDSPInstruction opc);
	void srr(const UDSPInstruction opc);
	void srrd(const UDSPInstruction opc);
	void srri(const UDSPInstruction opc);
	void srrn(const UDSPInstruction opc);
	void ilrr(const UDSPInstruction opc);
	void ilrrd(const UDSPInstruction opc);
	void ilrri(const UDSPInstruction opc);
	void ilrrn(const UDSPInstruction opc);

	// Arithmetic
	void clr(const UDSPInstruction opc);
	void clrl(const UDSPInstruction opc);
	void andcf(const UDSPInstruction opc);
	void andf(const UDSPInstruction opc);
	void tst(const UDSPInstruction opc);
	void tstaxh(const UDSPInstruction opc);
	void cmp(const UDSPInstruction opc);
	void cmpar(const UDSPInstruction opc);
	void cmpi(const UDSPInstruction opc);
	void cmpis(const UDSPInstruction opc);
	void xorr(const UDSPInstruction opc);
	void andr(const UDSPInstruction opc);
	void orr(const UDSPInstruction opc);
	void andc(const UDSPInstruction opc);
	void orc(const UDSPInstruction opc);
	void xorc(const UDSPInstruction opc);
	void notc(const UDSPInstruction opc);
	void xori(const UDSPInstruction opc);
	void andi(const UDSPInstruction opc);
	void ori(const UDSPInstruction opc);
	void addr(const UDSPInstruction opc);
	void addax(const UDSPInstruction opc);
	void add(const UDSPInstruction opc);
	void addp(const UDSPInstruction opc);
	void addaxl(const UDSPInstruction opc);
	void addi(const UDSPInstruction opc);
	void addis(const UDSPInstruction opc);
	void incm(const UDSPInstruction opc);
	void inc(const UDSPInstruction opc);
	void subr(const UDSPInstruction opc);
	void subax(const UDSPInstruction opc);
	void sub(const UDSPInstruction opc);
	void subp(const UDSPInstruction opc);
	void decm(const UDSPInstruction opc);
	void dec(const UDSPInstruction opc);
	void neg(const UDSPInstruction opc);
	void abs(const UDSPInstruction opc);
	void movr(const UDSPInstruction opc);
	void movax(const UDSPInstruction opc);
	void mov(const UDSPInstruction opc);
	void lsl16(const UDSPInstruction opc);
	void lsr16(const UDSPInstruction opc);
	void asr16(const UDSPInstruction opc);
	void lsl(const UDSPInstruction opc);
	void lsr(const UDSPInstruction opc);
	void asl(const UDSPInstruction opc);
	void asr(const UDSPInstruction opc);
	void lsrn(const UDSPInstruction opc);
	void asrn(const UDSPInstruction opc);
	void lsrnrx(const UDSPInstruction opc);
	void asrnrx(const UDSPInstruction opc);
	void lsrnr(const UDSPInstruction opc);
	void asrnr(const UDSPInstruction opc);

	// Multipliers
	void clrp(const UDSPInstruction opc);
	void tstprod(const UDSPInstruction opc);
	void movp(const UDSPInstruction opc);
	void movnp(const UDSPInstruction opc);
	void movpz(const UDSPInstruction opc);
	void addpaxz(const UDSPInstruction opc);
	void mulaxh(const UDSPInstruction opc);
	void mul(const UDSPInstruction opc);
	void mulac(const UDSPInstruction opc);
	void mulmv(const UDSPInstruction opc);
	void mulmvz(const UDSPInstruction opc);
	void mulx(const UDSPInstruction opc);
	void mulxac(const UDSPInstruction opc);
	void mulxmv(const UDSPInstruction opc);
	void mulxmvz(const UDSPInstruction opc);
	void mulc(const UDSPInstruction opc);
	void mulcac(const UDSPInstruction opc);
	void mulcmv(const UDSPInstruction opc);
	void mulcmvz(const UDSPInstruction opc);
	void maddx(const UDSPInstruction opc);
	void msubx(const UDSPInstruction opc);
	void maddc(const UDSPInstruction opc);
	void msubc(const UDSPInstruction opc);
	void madd(const UDSPInstruction opc);
	void msub(const UDSPInstruction opc);

private:
	// Global compilation state.
	llvm::LLVMContext& ctx;
	llvm::Module* mod;
	llvm::IRBuilder<> builder;
	llvm::FunctionPassManager* fpm;
	llvm::ExecutionEngine* ee;
	DSPLLVMCompiledCode compile_stub;

	// Compilation mutable state.
	u16 compile_pc;
	u16 current_block_start;
	u16 current_block_end;
	llvm::Value* elapsed_cycles;
	llvm::BasicBlock* dispatcher;
	llvm::BasicBlock* return_block;
	std::map<u16, llvm::BasicBlock*> instr_blocks;

	DSPLLVMCompiledCode GenerateStub();

	void EmitExceptionCheck(u16 elapsed_cycles);
	void EmitInstruction(UDSPInstruction inst);
	void EmitDispatcher();
	void EmitNonlocalDispatcher();

	void CheckIdleSkip();

	// LLVM utilities
	template <int bits, bool sign>
	llvm::ConstantInt* GenInt(u32 i)
	{
		llvm::IntegerType* ty = llvm::IntegerType::get(ctx, bits);
		return llvm::ConstantInt::get(ty, i, sign);
	}

	llvm::ConstantInt* GenUInt8(u8 i) { return GenInt<8, false>(i); }
	llvm::ConstantInt* GenUInt16(u16 i) { return GenInt<16, false>(i); }
	llvm::ConstantInt* GenUInt32(u32 i) { return GenInt<32, false>(i); }

	llvm::ConstantInt* GenSInt8(s8 i) { return GenInt<8, true>(i); }
	llvm::ConstantInt* GenSInt16(s16 i) { return GenInt<16, true>(i); }
	llvm::ConstantInt* GenSInt32(s32 i) { return GenInt<32, true>(i); }

	llvm::Value* GenFunctionCall(void* fptr, llvm::FunctionType* ft,
	                             const std::string& name,
	                             int nparams, ...);

	llvm::Value* GetCppInt(void* vptr, int bits, const std::string& name = "");

	void SetOptimizationPasses();
};

#endif // _DSPLLVMEMITTER_H
#endif // USE_LLVM
